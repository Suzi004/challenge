import React from "react";
import { Route, Switch, BrowserRouter, Redirect } from "react-router-dom";
import { PrivateRoute } from "./PrivateRoute";

// import ErrorBoundary from "./utils/ErrorBoundries.jsx";

import Login from "./pages/login/Login.jsx";
import AppBarMenu from "./pages/appBarMenu/AppBarMenu.jsx";


export default function MainRouter() {
  
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/login" component={Login} />
          <PrivateRoute strict path="/app" component={AppBarMenu} />
          <Route path="*" render={()=> <Redirect to={"/app/posts"} />} />
        </Switch>
      </BrowserRouter>
  );
}
