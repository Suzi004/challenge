import React, { PureComponent } from "react";
// import PropTypes from "prop-types";

import { Grid, Typography, Paper } from "@material-ui/core";

export default class CommentCard extends PureComponent {


  render() {
    const { message, date } = this.props;

    return (
        <Paper style={{border: '1px solid black'}}>
      <Grid
        item
        container
        direction="row"
        justify="space-around"
        alignItems="center"
      >
        <Grid item lg={10} xs={9} sm={9}>
          <Typography variant="body1" gutterBottom>
            Komentar glasi: {message}
          </Typography>
          <Typography variant="h5" gutterBottom>
            Datum : {date}
          </Typography>
        </Grid>
      </Grid>
      </Paper>
    );
  }
}