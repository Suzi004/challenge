import React, { Component } from "react";
import CommentCard from '../comments/CommentCard';

export default class Comments extends Component {
    state={
        posts:[]
    }

    componentDidMount(){
        let data = JSON. parse(localStorage.getItem("user"));
        // JSON. parse('{ "name":"John", "age":30, "city":"New York"}');
        this.setState({posts:data})
      }
   render(){
    const { posts } = this.state;

    let comments = [];
    for(let i=0;i<posts.length;i++)
    { console.log(posts[i]);
        comments = comments.concat(posts[i].comments)
        console.log(comments);
    }
   

    const listOfComments=comments.map((option, i) => (
        <CommentCard
          key={i}
          message={option.message}
          date={option.date}
        />
      )); 

        return (
            <div>
                this comments part
                {listOfComments}
            </div>
        );
        }
}