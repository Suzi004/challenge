import React from "react";
import { Route, Switch } from "react-router-dom";
import Comments from "../comments/Comments";
import Posts from "../posts/Posts"

export default function MenuRouter() {
  return (
    <Switch>
      <Route exact path="/app/comments" component={Comments} />
      <Route exact path="/app*" component={Posts} />
    </Switch>
  );
}