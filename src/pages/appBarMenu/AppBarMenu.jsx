import React, { PureComponent } from "react";

import AppBar from "@material-ui/core/AppBar";
import Grid from "@material-ui/core/Grid";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import Person from "@material-ui/icons/Person";
import MenuRouter from "./MenuRouter"

export default class AppBarDashbaord extends PureComponent {
  handleLogout = event => {
    localStorage.removeItem("user");
    this.props.history.push("/login");
  };

  handlePosts = event => {
   this.props.history.push("/app/posts");
  };
  handleComments = event => {
   this.props.history.push("/app/comments");
  };

  render() {
    return (
      <>
      <AppBar position="static">
        <Toolbar>
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
          >
            <Grid item>
              <Button color="inherit" onClick={this.handlePosts}>Posts</Button>
              <Button color="inherit" onClick={this.handleComments}>Comments</Button>
            </Grid>
            <Grid item>
              <IconButton edge="start" color="inherit" aria-label="menu">
                <Person />
              </IconButton>
              <Button color="inherit" onClick={this.handleLogout}>
                Logout
              </Button>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
      <MenuRouter />
      </>
    );
  }
}
