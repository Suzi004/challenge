import React, { PureComponent } from "react";
// import PropTypes from "prop-types";

import { Grid, Typography, Paper, IconButton } from "@material-ui/core";
import ThumbUpAltOutlinedIcon from '@material-ui/icons/ThumbUpAltOutlined';

import CommentCard from '../comments/CommentCard';


export default class PostsCard extends PureComponent {
//   static propTypes = {
//     title: PropTypes.string.isRequired,
//     positive: PropTypes.bool.isRequired,
//     message: PropTypes.string.isRequired

//   };

//   static defaultProps = {
//     title: "",
//     message: "",
//     numberOfLikes: 0
//   };

state = {
    numberOfLikes: this.props.numberOfLikes
}
handleLike = () => {
    this.props.handleLike(this.props.postNumber);
    let numberOfLikes = this.state.numberOfLikes+1;
    this.setState({numberOfLikes: numberOfLikes})
  };

  render() {
    const { title, message,numberOfLikes, handleLike, comments } = this.props;

    const listOfComments=comments.map((option, i) => (
        <CommentCard
          key={i}
          message={option.message}
          date={option.date}
        />
      )); 

    return (
    <Paper style={{border: '1px solid black'}}>
      <Grid
        item
        container
        direction="row"
        justify="space-around"
        alignItems="center"
      >
        <Grid item lg={10} xs={9} sm={9} >
          <Typography variant="h5" gutterBottom>
            {title}
          <IconButton aria-label="delete" color="primary" onClick={this.handleLike}  >
            <ThumbUpAltOutlinedIcon  />
          </IconButton>
          {this.state.numberOfLikes}
          </Typography>
          <Typography variant="body1" gutterBottom>
            {message}
          </Typography>
        </Grid>
        <Grid item lg={10} xs={9} sm={9} >
           {listOfComments}
        </Grid>
      </Grid>
      </Paper>
    );
  }
}