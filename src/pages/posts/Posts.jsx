import React, { Component } from "react";

import PostsCard from "./PostsCard";
import { Button } from "@material-ui/core";

export default class Posts extends Component {

    state = {
        posts: [
            {title: 'prvi post', message: 'this is an automated text msgs lorem ipsum .... ', numberOfLikes: 2, comments: [{message: 'Prvi komentar je ', date: '22/3/4'},{message: 'drugi komentar je ', date: '22/3/2020'} ]},
            {title: 'drugi post', message: 'this is an automated text msgs lorem ipsum .... ', numberOfLikes: 3,  comments: [{message: 'Neki tamo komentar je ', date: '23/4/2025'}]}
        ],
      };

      componentDidMount(){
        let data = this.state.posts;
        localStorage.setItem("user", JSON.stringify(data));
      }

    handleLike = (i) => {
          let increased= this.state.posts[i].numberOfLikes+1;
          this.state.posts[i].numberOfLikes=increased;
          let data = this.state.posts;
          this.setState({posts: data});
    }

   render(){

    const { posts } = this.state;
    
    let sumOfLikes=0;
    for (let i=0; i<posts.length; i++) {
        sumOfLikes += posts[i].numberOfLikes;
    }
    let sumOfComments = 0;
    for (let i=0; i<posts.length; i++) {
        sumOfComments += posts[i].comments.length;
    }

    const listOfPosts=posts.map((option, i) => (
          <PostsCard
            key={i}
            postNumber={i}
            title={option.title}
            numberOfLikes={option.numberOfLikes}
            message={option.message}
            handleLike = {this.handleLike}
            comments = {option.comments}
          />
        )); 
        return (
            <div>
               <br />
              <Button  type="submit" variant="contained" color="primary"> Reload </Button>
              <span> llikes: {sumOfLikes} </span>
              <span> comments: {sumOfComments} </span>
              <br />
              <br />
              {listOfPosts} 
            </div>
        );
        }
}

